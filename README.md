# The Development of Smart Contracts for Heterogeneous Blockchains

```
Henry Syahputra^1 and Hans Weigand^2
```
^1) hs@kontrak.io
^2) Tilburg University, P.O. Box 90153 5000 LE Tilburg, The Netherlands
weigand@uvt.nl

**Abstract.** 
The advance development in blockchain technologies at the present
time is sort of having to have external applications than interact with the native
blockchain. We can create and embed the business logic within the blockchain,
something called a smart contract. It allows us to put business logic which can
modify and share with multiple parties. This paper discusses on the development
process of smart contract platform that aim to create smart contract for hetero-
geneous blockchain technologies. We start the process with creating blueprint
design, Datalogical stereotype that employ the ontology design from Resources-
Event-Agent (REA) perspective [19]. With the modelling approach we are using
in this paper, Unified Modelling Language (UML) and Object Constraint Lan-
guage (OCL), we implement the workflow and algorithm in supply chain demo
sample.

**Keywords:**
Smart contracts, Blockchain, UML, OCL, Resources-Event-Agent
(REA).

## 1 Introduction

Contracts, transactions, and the records are among the defining structures in our eco-
nomic, legal, and political systems [23]. They protect assets and set organizational
boundaries. However, these tools and the bureaucracies formed to manage them have
not kept up with the economy’s digital transformation. Blockchain promises to solve
the problem by record the transactions between parties efficiently in a verifiable and
permanent way [30]. The ledger itself can also be programmed to trigger transactions
automatically [26] with what we know as smart contracts.
Although we share the enthusiasm for Blockchain potential, many business practitioners 
worry about this hype. It’s not just security issues [24] that concern us, various
technology stacks of a Blockchain [28], various blockchain technologies and the distributed 
governance [27] involves an ecosystem of agents, policies, services, and this
makes difficulties to develop Blockchain and smart contracts in particular. Direct impact
of one code into valuable assets stored in blockchain makes it vulnerable for manipulation 
attempts especially if the networks openly operate, restricted operated network however has also accidental failures[8][9]. 
Incidents, malfunctioning, and unexpected results of smart contracts have been reported [10][11][12].

The design of smart contracts aim to provide developers with model for creating
flexible and reusable applications. Security, evaluation and contribution are among the
critical aspects of smart contract development [7]. Blockchain from the commitment
based ontology perspective [1][2], differentiated into three levels, Essential, Infological
and Datalogical. In this approach, smart contracts constructed of with at least contains
of (1) goals, (2) commitments, (3) conditions, (4) actions, and (5) timing. We formulate
smart contracts into a standard modelling approach with MDA to perform the generation 
of smart contracts implementation with predefined paramaters [1].

In this paper, we propose a worklow and framework to develop smart contract in
independent way. We use the use case of the heterogeneous environment and scenarios
which resulted on how to come up with the ideal composition of smart contract from
practical point of view. This enables the developers to adopt a blockchain model-driven,
focusing on the logical design of a blockchain application rather than the complexity of
technical implementation of the various blockchain technologies and stacks.
To make a better understanding, in the Appendix section, we use a supply chain
example to fairly explain the process of creating smart contract [4], in [31] we illustrate
how a company transformed into blockchain by employing our stereotype. We will
generate the code skeleton for both ethereum and hyperledger fabric as our blockchain
target platforms in this demo example by using the workflow and framework we have
developed.

## 2 Background

In the software development phases cycle, models are being used and it is the main
preliminary key in Model Development Architecture [5]. The software development
process started from the definition of business process which later transform into business
process model and creating traceability links to the next process, analysis and design. 
The latter process will define on how the code generation works can be started.

![picture](/resources/table_1.png)


We will map the developed MDA design https://gitlab.com/Syahputra/Blockchain-
Toolbox-Eclipse-Update-Site https://gitlab.com/Syahputra/BlockchainToolbox-


Eclipse-Update-Site into a runnable smart contract code in our development environment
using (1) Unified Modelling Language that provides standard mechanisms [13]
and simplified metamodel for developers [15]. (2) OCL, a formal language used to de-
scribe expressions on UML [14] [7] with pre & post meta-level conditions. (3) Acceleo,
a code generator implementation of of the OMG's model-to-text (M2T) specification
[10]. (4) Blockchain Network, Fabric Hyperledger and Ethereum that are highly flexible [6][23].

## 3 Conceptual collaboration context

Stereotype is a class that defines how metaclass are being extended as part of a profile
[29]. Stereotype have properties that are referred to as tag definitions. Model element’s
value properties that are applied by stereotype referred to as tagged values. In Class
based redundancy [17], Stereotypes qualify specific classes and define component of
redundant structure. Tagged values, parameters that are assigned to elements of the
UML can be prescribed by stereotypes which later to be associated with UML elements.
The complete list of tagged values required for all types of elements can be found in
[18]. Our UML diagram use MDA with commitment based ontology [1] approach to
define a PIM. Once the PIM to PSM transformation is realized, the specific code of the
target platform is generated by using Acceleo M2T (Model to Text).

**3.1 Essential Interface**

Essential concerned with what is created directly or indirectly by communication [1].
The essential layer is similar to CIM. In order to get the general abstraction for the
smart contracts, we adapt the previous research [2], Enterprise Ontology with the Business
Ontology of with essential interface as depicted in table 2. The developed REA
model [19] being used as a domain ontology for accounting.
Thus, the essential interface represents economic events processes interface, the operations. 

The Operations of essential interface (Table 2) consists of, (1) totalSupply():
an operation to get the total token supply from the related blockchain. (2) balanceOf(),
the operation that aim to get the account balance related with a given blockchain address.
(3) transfer(): the operation to send economic events to a given destination that
relates with one’s address. (4) transferFrom(): the operation to get an economic events
from a given source of origin’s address in blockchain. We differentiate the transfer into
origin and destination as a way to capture the pre and post condition of an address in
real-time manner.

**3.2 Infological interface**

The infological interface concerned with value transfer between objects [1]. Logics of
value transfer functions agreed by contract participants are the foundation of commitments
at infological layer. The representation of business exchange represented by the
goals of each contract parties [25] and it contains of (1) goals, (2) commitments, (3)


conditions and (4) actions. While [1] added timing as additional goals, the infological
interface here use a changed(...) operation as a tagged values pointer to remark the time
changes. In this infological interface, timing are handled by the platform the difference
implementations of block creation time from heterogeneous blockchain platforms.
In this infological interface (Table 3), we define several operations: (1) reserve(): to
first use the infological we have to reserve a name for it. (2) owner(): once name is
reserved this operation will set who own the account. (3) transfer(): to transfer assets,
we change it by the name of the owner with this operation. (4) address(): this is the
primary address associated with a name that is similar to an A record in traditional
DNS. (5) content(): operations related with informations associated with owner. (6)
registrar(): any associated parties/peers related with sub registrar handled by this operation.
(7) disown(): to release current control of an associated name. (8) changed(): a
value pointer to remark the time changes. The owners of contract have reciprocal 
commitments, and it has to be reserved by pointing to a name owner first. Ownerships are
transferrable by referring to name owner. To remark changes in commitments we use
changed() operations. The commitment has to be balanced [20] and since it is part of
the smart contract abstraction, ownership and disown are very crucial for this particular
reason.

**3.3 Datalogical Profile**

The Smart contracts context are constructed at the datalogical level [3] where it is cre-
ated, operated and mapped into a targeted blockchain platform. We define <<Datalogical>> 
profile that provides a generic extension mechanism for customizing UML models. 
It starts from model initiation to transaction process on the targeted blockchain. 
We define the <<Datalogical>> profile by using stereotypes, tag definitions
that applies to UML specific model elements.

In figure 4, the <<Metaclass>> column specify the UML elements that are applicable
with our stereotypes. Furthermore, the <<Stereotype>> column consists of stereotypes
belongs to <<Datalogical>> profile where each respected stereotype correspondingly
owned functions and parameters in tagged values column. We inherit UML <<meta-class>> 
element and define our stereotypes (Figure 4) :

- **Operation** : Applicable stereotypes with this element are:(1) Event, (2) Function, (3)
    Modifier and (4) Constructor. All this stereotypes relate with the targeted programming
    language that orchestrate, manage, and manipulate the smart contracts code in
    the targeted blockchain platform.
- **Class** model element manages objects and features classifications as formal template
    parameter. Applicable stereotypes are: (1) Struct, (2) Contract and (3) Library. These
    stereotypes are characterized from its blockchain target language.
- **Constraint** element manage the semantics of elements by applying ParameterModifier
    stereotype.
- **Enumeration** element import all non private members belong to a package from the
    target blockchain by applying FunctionType stereotype


- **Interface** element connects with APIs of blockchain platforms (i.e Hyperledger 
    Fabric API, Ethereum API) by applying the DatalogicaI stereotype.
- **PackageImport** element connects with different kind of packages by applying 
    import stereotype and importFilter tagged values.
- **Parameter** element passes information from/to invocation of behavioralFeature by
    applying the indexed stereotype.
- **Property** element changes the semantics of how the property behaves. It also 
    exposes by overriding the getter/setter methods. By applying Getters and setters 
    stereotype with property element , we can allow different access levels of public 
    properties while still being protected.
- **ValueMapping** element represents key and value from the various target blockchain platform 
    by applying Mapping and KeyMapping stereotypes.

## 4 Model Framework Creation (Model to Text)

Model transformation is a compilation process which transforms a source model into
a target model [22]. We adopt Obeo’s acceleo model transformations [16] and code
generation script tools that makes the code generation trivial by template editors [21].
The top-level structure shows the model transformations of class and interface in the
Datalogical profile (Figure 1). In this diagram, we can see connection between each
interface class: Both of the Essential and Infological interfaces are connected to the
core datalogical by applying <<Datalogical>> profile. Simultaneously, to access these
interfaces, we apply the Essential and Infological types from the extended Datalogical
profile. In Appendix.1 we implement these relations in the supply chain demo example
where in the Quotation contract, the <<Essential>> type is applied by TransactionID
and FreightTerms while the other attributes, IssueDate and PaymentTerms implement
<<Infological>>type.

**4.1 Service Interface**

When we create a class diagram to model a solution, we have classes, interfaces, 
properties and operations. Service interface aim to classify and associate this classes to 
Essential, Infological and Datalogical. In algorithm 1, The SERVICE_INTERFACE
pseudocode supply an interface for transforming UML Model into targeted blockchain
code by using Acceleo scripts tools.
We adopt the UML Model as an input and responsible for populating dependencies
according to relationships usage and association at each particular class. By applying
this pseudocode, we take parameters, operations and attributes to generate a structure
composed by the attributes and pointers at the associated classes. For each related 
members of the datalogical stereotype (Table 4), the pseudocode will iteratively generate
the respected stereotype interface through all classes inside the UML model.
In the case where the GENERATE_BLOCKCHAIN_PACKAGE and
GENERATE_BLOCKCHAIN_INTERFACE templates are called (algorithm 2 and 3),
it will create the PSM related code implementations.

```
Algorithm 1: Interfacing UML Service
1:	template SERVICE_INTERFACE
2:	  for all class  <<Datalogical>>  do
3:		If all in parameters not empty  then
4:       GET_ESSENTIAL()
5:       GET_INFOLOGICAL()
6:      end if
7:    end for
8:	  for all class  Datalogical  do
9:      GENERATE_DATALOGICAL_STEREOTYPES()    
10:   end for

```

```
Algorithm 2: Generating Blockchain Package
1:	template GENERATE_SMART_CONTRACT_PACKAGE
2:       for class  packageOfContracts do
3:          GENERATE_CONTRACTS_DEPLOYER()
4:          GET_ALL_PUBLIC_CLASS_FUNCTIONS()
5:		    GENERATE_DIRECTORY_STRUCTURE()
6:       end for	


```

```
Algorithm 3: Generating Blockchain Interface
1:	template GENERATE_SMART_CONTRACT
2:       for classObject  ClassStereotype do
3:               GENERATE_STEREOTYPE_INTERFACE()
4:       end for	
```

**4.2 Code generation for target blockchain smart contracts**

The main purpose of code generation is to generate (1) smart contract package that
consists of source code with deployment script and (2) smart contract platform specific
implementation code. In algorithm 2, the GENERATE_BLOCKCHAIN_PACKAGE
template shows the pseudocode transformation for the targeted blockchain platform
into its targeted language. The template receives through iteration over the UML Model
that is being used to create smart contracts as the input. If class is a part of package
contracts of the targeted blockchain platform, it will create factory class by calling the
PRINT_CLASS_FACTORY. Moreover, it will create script for the smart contracts
package configuration, GENERATE_BUILD_SCRIPT and RUN_BUILD_SCRIPT.
This script will be executed by peer and is accessible via REST client (implemented by
i.e Ethereum and Hyperledger as client code) created by
GENERATED_REST_CLIENT.
The GENERATE_SMART_CONTRACT template produces source code that interacts
with core targeted blockchain. In algorithm 3, the operation paramaters will be
examined with (1) GET_INTERFACE_PREFIX(), (2) GET_NETWORK_CODE()


and (3) GET_UML_INTERFACE_SERVICE_CODE, that deals with targeted blockchains operations.

![picture](/resources/table_2.png)

![picture](/resources/table_3.png)

![picture](/resources/table_4.png)


![picture](/resources/fig_1.png)


## 5 Running The generated model

We want to use the generated source code in the target blockchain platform. Here, we
employ 2 different blockchain platforms as our target code generation, (1) Ethereum:
Generic permissionless blockchain platform and (2) Hyperledger: Modular permissioned
blockchain platform. In the previous section, we have applied the model to text
transformation that generate source code for ethereum and hyperledger fabric. We run
the ethereum generated .sol code using solidity remix browser (Figure 9). As for the
hyperledger fabric, the generated codes are packaged into maven structured directories.
We build each respected folders into a compiled archive files which consists of service
of classes and interfaces and run them on hyperledger fabric network (Figure 10).

## 6 Discussion

A methodology workflow for UML modeling to create smart contracts for heteregenous
blockchains using REA ontology, commitment based smart contracts and MDA 
guidelines, is illustrated in this paper. Moreover, compilable targeted blockchain platform
code resulted from the detail process of model transformation. We are using open
source tools Eclipse modelling tools that includes Papyrus and Acceleo in the compilable 
targeted blockchain platform code processes. Thus, we propose algorithms and
framework for this paper (Figure 6,7,8) to simplify the development process for 
heterogeneous blockchain platforms. In the upcoming paper we expect to involve the study
of orchestrating modular blockchain network and incorporate them into the methodology
workflow presented in this paper.


![picture](/resources/fig_2.png)

![picture](/resources/fig_3.png)


**References**

1. De Kruijff J. Weigand H. An Ontology for Commitment Based Smart Contracts. 2017.
2. De Kruijff J. Weigand H. Towards a Blockchain Ontology. 2017.
3. De Kruijff, J., Weigand, H,: Understanding the Blockchain Using Enterprise Ontology,
    29th Int. Conference on Advanced Information Systems Engineering, 12-16 June 2017.
4. Omran Y. Henke M. Heines R. Hofmann E. Blockchain-driven supply chain finance: To-
    wards a conceptual framework from a buyer perspective. 2017
5. OMG-MDA, MDA Guide revision 1.0, OMG, (2003).
6. Valenta M. Sandner P. Comparison of Ethereum, Hyperledger Fabric and Corda.
7. Luu L. Chu D. Saxena P. Olickel H. Hobor A. Making Smart Contracts Smarter
8. J. C. Corbett et al. Spanner: Google’s globally distributed database. ACM Trans. Comput.
    Syst., aug 2013.
9. J. Baker et al. Megastore: Providing scalable, highly available storage for interactive ser-
    vices. In CIDR, pages 223–234, 2011.
10. EtherDice smart contract is down for maintenance. https: //www.red-
dit.com/r/ethereum/comments/47f028/ etherdice_is_down_for_maintenance_we_are_hav-
ing/.
11. KingOfTheEtherThrone smart contract. https: //github.com/kieranelby/KingOfTheEther-
Throne/ blob/v0.4.0/contracts/KingOfTheEtherThrone.sol.
12. GovernMental’s 1100 ETH payout is stuck because it uses too much gas. https: //www.red-
dit.com/r/ethereum/comments/4ghzhv/ governmentals_1100_eth_jackpot_pay-
out_is_stuck/.
13. Bondavalli A. Majzik I. Mura I. Automatic Dependability Analysis for Supporting Design
Decisions in UML.
14. Pollet D. Vojtisek D. Jezequel JM. OCL as a Core UML Transformation Language


15. Cariou E. Marvie R. Seinturier L. Duchien L. OCL for the Specification of Model Transfor-
    mations Contracts.
16. Acceleo: https://wiki.eclipse.org/Acceleo
17. J. Xu, B. Randell, C. M. F. Rubira-Calsavara and R. J. Stroud, "Towards an object-oriented
    approch to software fault-tolerance," University of Newcastle Upon Tyne, PDCS-2 Tech-
    nical Report, 1994.
18. A. Bondavalli, I. Majzik and I. Mura, "From structural UML diagrams to Timed Petri Nets,"
    European ESPRIT Project 27439 HIDE, Deliverable 2, Section 4, 1998.
19. McCarthy W.: The REA Accounting Model: A Generalized Framework for Accounting Sys-
    tems in a Shared Data Environment, The Accounting Review, Vol. LVII, No. 3, July 1982
20. Chopra, A.K., Singh M.P., Oren, N., Miles, S., Luck, M., Modgil, S., Desai, N.: Analyzing
    Contract Robustness through a Model of Commitments, Agent-Oriented Software Engi-
    neering XI, LNCS 6788 pp 17-36, 2011
21. Obeo. Acceleo - Model to Text transformation, 2010.
22. Antonio Wendell De Oliveira Rodrigues, Fr ́ed ́eric Guyomarc’H, Jean-Luc Dekeyser. An
    MDE Approach for Automatic Code Generation from MARTE to OpenCL. [Research Re-
    port] RR- 7525, INRIA. 2011, pp.
23. Lansiti M. Lakhani K. The Truth About Blockchain. January-February 2017 issue (pp.118-
    127). Harvard Business Review.
24. The inside story of MT.Gox, Bitcoin’s $460 million disaster.
    https://www.wired.com/2014/03/bitcoin-exchange/
25. N. Szabo. Formalizing and securing relationships on public networks. First Mon- day, 2(9),
    1997
26. M. Swan. Blockchain thinking: The brain as a dac (decentralized autonomous organization).
    In Texas Bitcoin Conference, pages 27–29, 2015.
27. Norta A. Establishing Distributed Governance Infrastructures for Enacting Cross-Organiza-
    tion Collaborations.
28. Herrera-Joancomartí J., Pérez-Solà C. (2016) Privacy in Bitcoin Transactions: New Chal-
    lenges from Blockchain Scalability Solutions. In: Torra V., Narukawa Y., Navarro-Arribas
    G., Yañez C. (eds) Modeling Decisions for Artificial Intelligence. MDAI 2016. Lecture
    Notes in Computer Science, vol 9880. Springer, Cham
29. UML Stereotype. https://www.uml-diagrams.org/stereotype.html
30. Weigand, H. Value encounters – Modelling and Analyzing co-creation value, 2009
31. https://gitlab.com/Syahputra/SupplyChain-Blockchain-Demo-Tilburg
32. https://gitlab.com/Syahputra/BlockchainToolbox-Eclipse-Update-Site

## How To Install This Plugin

![Alt Text](/resources/BlockchainToolbox_Install_plugin.gif)


## Appendix

In this sample, we explain the use of blockchain in the supply chain. Narratively, the
purchase request developed and sent to the purchasing and the financial departments.
When the two departments approve the purchase request, they send to the suppliers a
request for quotations. The Quotation adopts <<Contract>> stereotype and considered
as a smart contracts. For more details, the supply chain demo is accessible 
via [31] and [32]




